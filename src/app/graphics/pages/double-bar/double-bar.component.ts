import { Component } from '@angular/core';
import { ChartDataSets } from 'chart.js';

@Component({
  selector: 'app-double-bar',
  templateUrl: './double-bar.component.html',
  styles: [],
})
export class DoubleBarComponent {
  providersData: ChartDataSets[] = [
    { data: [100, 200, 300, 400, 500], label: 'Vendedor A' },
    { data: [50, 250, 30, 450, 200], label: 'Vendedor B' },
  ];
  providersLabels: string[] = ['2021', '2022', '2023', '2024', '2025'];
  productData: ChartDataSets[] = [
    {
      data: [200, 300, 400, 300, 120],
      label: 'Carros',
    },
  ];
}
