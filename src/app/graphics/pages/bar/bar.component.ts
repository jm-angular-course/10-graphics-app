import { Component } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styles: [],
})
export class BarComponent {
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      },
    },
  };
  public barChartLabels: Label[] = [
    '2006',
    '2007',
    '2008',
    '2009',
    '2010',
    '2011',
    '2012',
  ];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    {
      data: [65, 59, 80, 81, 56, 55, 40],
      label: 'Series A',
    },
    {
      data: [28, 48, 40, 19, 86, 27, 90],
      label: 'Series B',
    },
    {
      data: [11, 42, 34, 66, 43, 11, 33],
      label: 'Series C',
    },
  ];

  constructor() {}

  public randomize(): void {
    // Only Change 3 values
    this.barChartData.forEach((charData) => {
      charData.data = this.generateRandomChartData();
    });
  }

  private generateRandomChartData() {
    return [
      (Math.random() * 100) | 0,
      (Math.random() * 100) | 0,
      (Math.random() * 100) | 0,
      (Math.random() * 100) | 0,
      (Math.random() * 100) | 0,
      (Math.random() * 100) | 0,
      (Math.random() * 100) | 0,
    ];
  }
}
