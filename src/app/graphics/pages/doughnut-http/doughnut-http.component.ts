import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';
import { GraphicsService } from '../../services/graphics.service';

@Component({
  selector: 'app-doughnut-http',
  templateUrl: './doughnut-http.component.html',
  styles: [],
})
export class DoughnutHttpComponent implements OnInit {
  public readonly doughnutChartType: ChartType = 'doughnut';
  public loading: boolean = true;
  public doughnutChartLabels: Label[] = [];
  public doughnutChartData: SingleDataSet = [];

  constructor(private graphicsService: GraphicsService) {}

  ngOnInit(): void {
    this.graphicsService
      .getDoughnutSocialMediaData()
      .subscribe(({ labels, values }) => {
        this.doughnutChartLabels = labels;
        this.doughnutChartData = values;
        this.loading = false;
      });
  }
}
