import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { BarGraphicComponent } from './components/bar-graphic/bar-graphic.component';
import { GraphicsRoutingModule } from './graphics-routing.module';
import { BarComponent } from './pages/bar/bar.component';
import { DoubleBarComponent } from './pages/double-bar/double-bar.component';
import { DoughnutHttpComponent } from './pages/doughnut-http/doughnut-http.component';
import { DoughnutComponent } from './pages/doughnut/doughnut.component';

@NgModule({
  declarations: [
    BarComponent,
    DoubleBarComponent,
    DoughnutComponent,
    DoughnutHttpComponent,
    BarGraphicComponent,
  ],
  imports: [CommonModule, GraphicsRoutingModule, ChartsModule],
})
export class GraphicsModule {}
