import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BarComponent } from './pages/bar/bar.component';
import { DoubleBarComponent } from './pages/double-bar/double-bar.component';
import { DoughnutHttpComponent } from './pages/doughnut-http/doughnut-http.component';
import { DoughnutComponent } from './pages/doughnut/doughnut.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'barra', component: BarComponent },
      { path: 'barra-doble', component: DoubleBarComponent },
      { path: 'dona', component: DoughnutComponent },
      { path: 'dona-http', component: DoughnutHttpComponent },
      { path: '**', redirectTo: 'barra' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GraphicsRoutingModule {}
