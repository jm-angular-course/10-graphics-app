import { Component } from '@angular/core';

class MenuItem {
  constructor(public route: string, public text: string) {}
}

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styles: [
    `
      li {
        cursor: pointer;
      }
    `,
  ],
})
export class MenuComponent {
  menuItems: MenuItem[] = [
    new MenuItem('graficas/barra', 'Barras'),
    new MenuItem('graficas/barra-doble', 'Barras Dobles'),
    new MenuItem('graficas/dona', 'Dona'),
    new MenuItem('graficas/dona-http', 'Dona Http'),
  ];
}
